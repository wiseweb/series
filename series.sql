SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

CREATE DATABASE IF NOT EXISTS `series` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `series`;

DROP TABLE IF EXISTS `episodes`;
CREATE TABLE IF NOT EXISTS `episodes` (
  `id` int(10) unsigned NOT NULL,
  `originUrl` varchar(255) NOT NULL,
  `epNumber` int(10) unsigned NOT NULL,
  `epName` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `watched` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(4) DEFAULT NULL COMMENT '0-oldest;1-newest'
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

INSERT INTO `episodes` (`id`, `originUrl`, `epNumber`, `epName`, `timestamp`, `watched`, `status`) VALUES
(19, 'http://www.topserialy.sk/serialy/agents-of-shield-s04e02', 2, 'Meet the New Boss', '2016-10-07 09:22:30', 0, 1),
(20, 'http://www.topserialy.sk/serialy/agents-of-shield-s04e01', 1, 'The Ghost', '2016-10-07 09:22:32', 0, NULL),
(21, 'http://www.topserialy.sk/serialy/agents-of-shield-s03e22', 22, 'Ascension (2)', '2016-10-07 09:22:35', 0, NULL),
(22, 'http://www.topserialy.sk/serialy/agents-of-shield-s03e21', 21, 'Absolution (1)', '2016-10-07 09:22:37', 0, NULL),
(23, 'http://www.topserialy.sk/serialy/agents-of-shield-s03e20', 20, 'Emancipation', '2016-10-07 09:22:39', 0, NULL),
(24, 'http://www.topserialy.sk/serialy/agents-of-shield-s03e19', 19, 'Failed Experiments', '2016-10-07 09:22:41', 0, NULL),
(25, 'http://www.topserialy.sk/serialy/agents-of-shield-s03e18', 18, 'The Singularity', '2016-10-07 09:22:44', 0, NULL),
(26, 'http://www.topserialy.sk/serialy/agents-of-shield-s03e17', 17, 'The Team', '2016-10-07 09:22:46', 0, NULL),
(27, 'http://www.topserialy.sk/serialy/agents-of-shield-s03e16', 16, 'Paradise Lost', '2016-10-07 09:22:49', 0, NULL),
(28, 'http://www.topserialy.sk/serialy/arrow-s05e01', 1, 'Legacy', '2016-10-07 09:31:05', 0, 1),
(29, 'http://www.topserialy.sk/serialy/arrow-s04e23', 23, 'Schism', '2016-10-07 09:31:08', 0, NULL),
(30, 'http://www.topserialy.sk/serialy/arrow-s04e22', 22, 'Lost in the Flood', '2016-10-07 09:31:10', 0, NULL),
(31, 'http://www.topserialy.sk/serialy/arrow-s04e21', 21, 'Monument Point', '2016-10-07 09:31:12', 0, NULL),
(32, 'http://www.topserialy.sk/serialy/arrow-s04e20', 20, 'Genesis', '2016-10-07 09:31:15', 0, NULL),
(33, 'http://www.topserialy.sk/serialy/arrow-s04e19', 19, 'Canary Cry', '2016-10-07 09:31:17', 0, NULL);

DROP TABLE IF EXISTS `serials`;
CREATE TABLE IF NOT EXISTS `serials` (
  `id` int(10) unsigned NOT NULL,
  `serialName` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

INSERT INTO `serials` (`id`, `serialName`) VALUES
(6, 'Agents of shield'),
(7, 'Arrow');

DROP TABLE IF EXISTS `series`;
CREATE TABLE IF NOT EXISTS `series` (
  `id` int(10) unsigned NOT NULL,
  `serieNum` int(10) unsigned NOT NULL,
  `serial_id` int(10) unsigned DEFAULT NULL,
  `episode_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

INSERT INTO `series` (`id`, `serieNum`, `serial_id`, `episode_id`) VALUES
(6, 4, 6, 19),
(7, 4, 6, 20),
(8, 3, 6, 21),
(9, 3, 6, 22),
(10, 3, 6, 23),
(11, 3, 6, 24),
(12, 3, 6, 25),
(13, 3, 6, 26),
(14, 3, 6, 27),
(15, 5, 7, 28),
(16, 4, 7, 29),
(17, 4, 7, 30),
(18, 4, 7, 31),
(19, 4, 7, 32),
(20, 4, 7, 33);

DROP TABLE IF EXISTS `sources`;
CREATE TABLE IF NOT EXISTS `sources` (
  `id` int(10) unsigned NOT NULL,
  `episode_id` int(10) unsigned NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

INSERT INTO `sources` (`id`, `episode_id`, `url`) VALUES
(6, 19, 'http://www.flashx.tv/embed-d56p1zcnk9g6-720x405.html'),
(7, 20, 'http://www.flashx.tv/embed-uyycr4bukgju-720x405.html'),
(8, 21, 'http://www.flashx.tv/embed-muilyivmc0la-720x405.html'),
(9, 22, 'http://www.flashx.tv/embed-o4vy3db2tk18-720x405.html'),
(10, 23, 'http://www.flashx.tv/embed-2xqkp6sd6512-720x405.html'),
(11, 24, 'http://www.flashx.tv/embed-sh10y49x95qv-720x405.html'),
(12, 25, 'http://www.flashx.tv/embed-b4nds0arv6mo-720x405.html'),
(13, 26, 'http://www.flashx.tv/embed-6ja1zr6p1osz-720x405.html'),
(14, 27, 'http://www.flashx.tv/embed-xob4dawf00x3-720x405.html'),
(16, 28, 'http://www.flashx.tv/embed-wnn09seflzxf-720x405.html'),
(17, 29, 'http://www.flashx.tv/embed-94lugy3ixt0q-720x405.html'),
(18, 30, 'http://www.flashx.tv/embed-4u35wlkgkhmj-720x405.html'),
(19, 31, 'http://www.flashx.tv/embed-ipokctk9p9yd-720x405.html'),
(20, 32, 'http://www.flashx.tv/embed-3d5ckj1edl3d-720x405.html'),
(21, 33, 'http://www.flashx.tv/embed-58hln53tqea4-720x405.html');


ALTER TABLE `episodes`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `serials`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`serialName`),
  ADD KEY `id` (`id`);

ALTER TABLE `series`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `serieNum` (`serieNum`,`episode_id`),
  ADD KEY `episode_id` (`episode_id`),
  ADD KEY `serial_id` (`serial_id`);

ALTER TABLE `sources`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `link` (`url`),
  ADD KEY `episode_id` (`episode_id`);


ALTER TABLE `episodes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
ALTER TABLE `serials`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
ALTER TABLE `series`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
ALTER TABLE `sources`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;

ALTER TABLE `series`
  ADD CONSTRAINT `series_ibfk_1` FOREIGN KEY (`episode_id`) REFERENCES `episodes` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `series_ibfk_2` FOREIGN KEY (`serial_id`) REFERENCES `serials` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
