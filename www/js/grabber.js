$(function(){
    function b64EncodeUnicode(str) {
        return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function(match, p1) {
            return String.fromCharCode('0x' + p1);
        }));
    }
    function ajaxGrab(url,done=[]){
        if($.inArray(url,done)>-1) {return;}

        var timeout=2000;
        var encoded_url=b64EncodeUnicode(url);
        encoded_url=encoded_url.replace(/\=+$/,'');
        $("#results").append("<div id='"+encoded_url+"'><span>"+url+"</span></div>");
        setTimeout(function () {
            $.nette.ajax({
                url: '?do=grab',
                method: "post",
                async: false,
                data: { url: url},
                complete: function(payload){
                    if(payload.status==200){
                        var serial=payload.responseJSON.response;
                        //console.log(serial);
                        if(typeof serial =='object')
                        {
                            if(serial.result=="success"){
                                $("#"+encoded_url).addClass('success');
                                $("#"+encoded_url).append(' - Added:'+serial.seriesTitle+' s'+serial.serieNum+"e"+serial.epNum+' - '+serial.epTitle);
                            }
                            else{
                                $("#"+encoded_url).addClass('error');
                                $("#"+encoded_url).append(' - '+serial.result);
                            }
                            if(serial.nextEp!=""){
                                ajaxGrab(serial.nextEp,done);
                            }
                            if(serial.previousEp!=""){
                                ajaxGrab(serial.previousEp,done);
                            }
                        }
                        else{
                            $("#"+encoded_url).addClass('error');
                            $("#"+encoded_url).append(' - '+serial);
                        }
                    }
                    else{
                        $("#"+encoded_url).addClass('error');
                        $("#"+encoded_url).append(' - Ajax failed');
                    }
                }
            });
        }, timeout);
        done.push(url);
    }
    $( "#frm-grabberForm :submit" ).click( function(e) {
        e.preventDefault();
        var form = $("#frm-grabberForm");
        if(Nette.validateForm(form[0])){
            ajaxGrab(form.find("#frm-grabberForm-url").val());
        }
    });

});
