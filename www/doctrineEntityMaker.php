<?php

use Doctrine\ORM\Tools\EntityGenerator;

ini_set('display_errors', 'On');
umask(0);
// this is not necessary if you use Doctrine2 with PEAR
//$libPath = __DIR__ . '/../lib/doctrine2';

// autoloaders
require __DIR__.'/../vendor/autoload.php';

//$classLoader = new \Doctrine\Common\ClassLoader('Doctrine', $libPath);	// custom path
$classLoader = new \Doctrine\Common\ClassLoader('Doctrine');    // with PEAR
$classLoader->register();

$classLoader = new \Doctrine\Common\ClassLoader('Entities', __DIR__);
$classLoader->register();
$classLoader = new \Doctrine\Common\ClassLoader('Proxies', __DIR__);
$classLoader->register();

// config
$config = new \Doctrine\ORM\Configuration();
$config->setMetadataDriverImpl($config->newDefaultAnnotationDriver(__DIR__.'/Entities'));
$config->setMetadataCacheImpl(new \Doctrine\Common\Cache\ArrayCache());
$config->setProxyDir(__DIR__.'/Series'); //CHANGE ME TO PROJECT FOLDER
$config->setProxyNamespace('Proxies');

$connectionParams = array( //CHANGE ME TO DATABASE SETTINGS
  'dbname' => 'series',
  'user' => 'root',
  'password' => '',
  'host' => 'localhost',
  'driver' => 'pdo_mysql',
);

$em = \Doctrine\ORM\EntityManager::create($connectionParams, $config);

// custom datatypes (not mapped for reverse engineering)
$em->getConnection()->getDatabasePlatform()->registerDoctrineTypeMapping('set', 'string');
$em->getConnection()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

// fetch metadata
$driver = new \Doctrine\ORM\Mapping\Driver\DatabaseDriver(
  $em->getConnection()->getSchemaManager()
);

$em->getConfiguration()->setMetadataDriverImpl($driver);
$cmf = new \Doctrine\ORM\Tools\DisconnectedClassMetadataFactory();
$cmf->setEntityManager($em);    // we must set the EntityManager

$classes = $driver->getAllClassNames();
$metadata = array();
foreach ($classes as $class) {
    //any unsupported table/schema could be handled here to exclude some classes
  if (true) {
      $metadata[] = $cmf->getMetadataFor($class);
  }
}

$generator = new EntityGenerator();
$generator->setAnnotationPrefix('ORM\\');   // edit: quick fix for No Metadata Classes to process
$generator->setUpdateEntityIfExists(true);    // only update if class already exists
//$generator->setRegenerateEntityIfExists(true);	// this will overwrite the existing classes
$generator->setGenerateStubMethods(true);
$generator->setGenerateAnnotations(true);
$generator->generate($metadata, __DIR__.'/../app/entities');

echo 'Done!';
