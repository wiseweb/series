<?php

namespace App\Models;

class TopSerialyGrabber
{
    public function grabURL($url)
    {
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            return false;
        }

        $content = $this->getContent($url);
        if (!$data = $this->parseHtml($content)) {
            return false;
        }
        $data->originUrl = $url;

        return $data;
    }
    private function getContent($url)
    {
        $agent = 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_3_3 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8J2 Safari/6533.18.5';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $agent);
        curl_setopt($ch, CURLOPT_URL, $url);
        $result = curl_exec($ch);

        return $result;
    }
    private function parseHtml($html)
    {
        $data = new \stdClass();
        if (!$sources = $this->getSources($html)) {
            return false;
        }
        $data->sources = $sources;
        $data->epTitle = $this->getEpTitle($html);
        $parsedTitle = $this->parseTitle($html);
        $data->seriesTitle = $parsedTitle['seriesName'];
        $data->serieNum = $parsedTitle['serieNum'];
        $data->epNum = $parsedTitle['epNum'];
        $data->nextEp = $this->getNeighbour($html, 'novae');
        $data->previousEp = $this->getNeighbour($html, 'stare');

        return $data;
    }
    private function getSources($html)
    {
        $sources = array();
        if (!preg_match_all("/<iframe.*src=.*<\/iframe>/", $html, $iframes)) {
            return false;
        }
        foreach ($iframes[0] as $iframe) {
            if (!preg_match('/src=[\'|"].*?[\'|"]/', $iframe, $srcs)) {
                continue;
            }
            $src = rtrim(substr($srcs[0], 5), '"');
            if (filter_var($src, FILTER_VALIDATE_URL)) {
                $sources[] = $src;
            }
        }

        return $sources;
    }
    private function getNeighbour($html, $way)
    {
        $link = '';
        if (preg_match('#<a.*class=[\'|"]'.$way.' *.*[\'|"]>.*<\/a>#', $html, $links)) {
            $pos = strpos($html, $links[0]);
            $arround = substr($html, $pos - 15, 20);
            if (strpos($arround, '<!--') === false) {
                if (preg_match('/href=[\'|"].*?[\'|"]/', $links[0], $href)) {
                    $href[0] = 'http://www.topserialy.sk'.rtrim(substr($href[0], 6), '"');
                    if (filter_var($href[0], FILTER_VALIDATE_URL)) {
                        $link = $href[0];
                    }
                }
            }
        }

        return $link;
    }
    private function parseTitle($html)
    {
        $return = array();
        $title = '';
        if (preg_match('#<h1.*class=[\'|"]h1epizoda *.*[\'|"]>.*<\/h1>#', $html, $titles)) {
            $title = strip_tags($titles[0]);
        }
        $title = explode('-', $title);
        $numbers = array_pop($title);
        $return['seriesName'] = rtrim(implode('-', $title));
        if (preg_match('/ ?s\d{1,2}/', $numbers, $serieNums)) {
            $serieNum = substr(ltrim($serieNums[0]), 1);
        }
        $return['serieNum'] = $serieNum;
        if (preg_match('/ ?e\d{1,2}/', $numbers, $epNums)) {
            $epNum = substr(ltrim($epNums[0]), 1);
        }
        $return['epNum'] = $epNum;

        return $return;
    }
    private function getEpTitle($html)
    {
        $title = '';
        if (preg_match('#<h2.*class=[\'|"]h2epname *.*[\'|"]>.*<\/h2>#', $html, $titles)) {
            $title = strip_tags($titles[0]);
        }

        return $title;
    }
}
