<?php

namespace App\Models;

class SerialModel
{
    public function buildSerieArray(\Nette\Database\ResultSet $data)
    {
        $result = [];
        foreach ($data as $episode) {
            $result[$episode->serieNum][$episode->epNumber] = $episode;
        }
        array_walk($result, 'krsort');
        krsort($result);

        return $result;
    }
}
