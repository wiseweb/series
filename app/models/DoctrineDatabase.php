<?php

namespace App\Models;

class DoctrineDatabase
{
    /**
     * @var \Kdyby\Doctrine\EntityManager
     */
    private $em;
    public function __construct(\Kdyby\Doctrine\EntityManager $em)
    {
        $this->em = $em;
    }
    public function saveEpisode($data)
    {
        $sources = [];
        foreach ($data->sources as $source) {
            $src = new \App\Entities\Sources();
            $src->setLink($source);
            $sources[] = $src;
        }

        $ep = new \App\Entities\Episodes();
        $ep->setEpisodeNumber($data->epNum);
        $ep->setName($data->epTitle);
        //$this->em->persist($ep);
        //$this->em->flush();
    }
}
