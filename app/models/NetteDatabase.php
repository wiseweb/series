<?php

namespace App\Models;

class NetteDatabase
{
    private $db;
    public function __construct(\Nette\Database\Context $db)
    {
        $this->db = $db;
    }
    public function favor($serialId, $userId)
    {
        $this->db->query("INSERT IGNORE INTO `myserials` VALUES($userId,$serialId)");
    }
    public function unfavor($serialId, $userId)
    {
        $this->db->query("DELETE FROM `myserials` WHERE `serial_id`=$serialId AND `user_id`=$userId");
    }
    public function getMySerials($userId)
    {
        $sql = "SELECT se.* FROM serials se
        LEFT JOIN myserials m ON se.id=m.serial_id
        WHERE m.user_id=$userId";

        return $this->db->query($sql)->fetchAll();
    }
    public function getSerials()
    {
        $sql = 'SELECT se.*,m.* FROM serials se
        LEFT JOIN myserials m ON se.id=m.serial_id';

        return $this->db->query($sql)->fetchAll();
    }
    public function setWatchedEpisode($episodeId, $userId, $serialId)
    {
        $this->db->query('INSERT IGNORE INTO `watched`', ['user_id' => $userId, 'episode_id' => $episodeId]);
        $this->db->query('INSERT IGNORE INTO `myserials`', ['user_id' => $userId, 'serial_id' => $serialId]);
    }
    public function setUnwatchedEpisode($episodeId, $userId)
    {
        $this->db->query("DELETE FROM `watched` WHERE `episode_id`=$episodeId AND `user_id`=$userId");
    }

    public function getEpisode($id, $userId = 'NULL')
    {
        $sql = "SELECT se.id AS serialId,se.serialName,s.id AS serieId,s.serieNum,e.id as episodeId,e.epName,e.epNumber,sr.url,w.user_id as watched FROM sources sr
        LEFT JOIN series s ON s.episode_id=sr.episode_id
        LEFT JOIN serials se ON se.id=s.serial_id
        LEFT JOIN episodes e ON e.id=sr.episode_id
        LEFT JOIN watched w ON w.episode_id=sr.episode_id AND w.user_id=$userId
        WHERE sr.episode_id=$id";

        return $this->db->query($sql)->fetchAll();
    }
    public function getNewUnwatchedEpisodes($id)
    {
        $sql = "SELECT se.id AS serialId,se.serialName,s.id AS serieId,s.serieNum,e.id as episodeId,e.epName,e.epNumber,w.user_id as watched FROM myserials m
            LEFT JOIN series s ON s.serial_id=m.serial_id
            LEFT JOIN serials se ON se.id=m.serial_id
            LEFT JOIN episodes e ON e.id=s.episode_id
            LEFT JOIN watched w ON w.episode_id=s.episode_id AND w.user_id=$id
            WHERE m.user_id=$id AND w.user_id IS NULL
            ORDER BY e.timestamp DESC,serieNum DESC, epNumber DESC";

        return $this->db->query($sql);
    }
    public function getEpisodesSmall($where = '')
    {
        $sql = 'SELECT se.id AS serialId,se.serialName,s.id AS serieId,s.serieNum,e.id as episodeId,e.epName,e.epNumber FROM episodes e
        LEFT JOIN series s ON s.episode_id=e.id
        LEFT JOIN serials se ON se.id=s.serial_id ';
        $sql .= $where;

        return $this->db->query($sql);
    }
    public function getEpisodes($where = 1)
    {
        return $this->db->table('episodes')->where($where);
    }
    public function saveEpisode($data, $userId)
    {
        try {
            $serial_id = $this->db->query('SELECT `id` FROM `serials` WHERE `serialName`=?', $data->seriesTitle)->fetchField();
            if (empty($serial_id)) {
                $this->db->query('INSERT INTO `serials`', ['serialName' => $data->seriesTitle]);
                $serial_id = $this->db->getInsertId();
            }

            $this->db->beginTransaction();
            $epArray = ['originUrl' => $data->originUrl, 'epName' => $data->epTitle, 'epNumber' => $data->epNum];
            if (empty($data->nextEp)) {
                $epArray['status'] = 1;
            }
            if (empty($data->previousEp)) {
                $epArray['status'] = 0;
            }
            $this->db->query('INSERT INTO `episodes`', $epArray);
            $ep_id = $this->db->getInsertId();
            $sources = [];
            foreach ($data->sources as $source) {
                $sources[] = array('episode_id' => $ep_id, 'url' => $source);
            }
            $this->db->query('INSERT INTO `sources`', $sources);
            $this->db->query('INSERT INTO `series`', ['serieNum' => $data->serieNum, 'serial_id' => $serial_id, 'episode_id' => $ep_id]);
            if ($userId) {
                $this->db->query('INSERT IGNORE INTO `myserials`', ['user_id' => $userId, 'serial_id' => $serial_id]);
            }
        } catch (\Nette\Database\DriverException $e) {
            switch ($e->getDriverCode()) {
                case 1062: return 'Duplicate entry';break;
                default: return 'Error saving to database - '.$e->getMessage().'<br>'.$e->getQueryString();break;
            }
            $this->db->rollBack();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        $this->db->commit();

        return true;
    }
}
