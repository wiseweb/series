<?php

namespace App;

use Nette;

class Facade extends Nette\Object
{
    private $db;
    private $models;
    public function __construct(Models\NetteDatabase $db)
    {
        $this->db = $db;
    }

    public function getSerials()
    {
        return $this->db->getSerials();
    }
    public function getMySerials($userId)
    {
        return $this->db->getMySerials($userId);
    }

    public function getEpisode($id, $userId)
    {
        return $this->db->getEpisode($id, $userId);
    }
    public function getNewUnwatchedEpisodes($id)
    {
        return $this->db->getNewUnwatchedEpisodes($id);
    }
    public function getSerialById($id)
    {
        $episodes = $this->db->getEpisodesSmall("WHERE se.id=$id");
        $model = $this->loadModel('SerialModel');
        $serieArray = $model->buildSerieArray($episodes);

        return $serieArray;
    }
    public function getAllEpisodes()
    {
        return $this->db->getEpisodes();
    }

    /** HANDLERS */
    public function favor(int $serialId, int $userId)
    {
        $this->db->favor($serialId, $userId);
    }
    public function unfavor(int $serialId, int $userId)
    {
        $this->db->unfavor($serialId, $userId);
    }
    public function markAsWatched(int $episodeId, int $userId, int $serialId = null)
    {
        $this->db->setWatchedEpisode($episodeId, $userId, $serialId);
    }
    public function markAsUnWatched(int $episodeId, int $userId)
    {
        $this->db->setUnwatchedEpisode($episodeId, $userId);
    }

    public function grabURL($url, $userId = null)
    {
        $model = $this->loadModel('TopSerialyGrabber');
        $data = $model->grabURL($url);
        if ($data === false) {
            return $data->result = "Doesn't exist";
        }
        $error = $this->db->saveEpisode($data, $userId);
        if ($error === true) {
            $data->result = 'success';
        } else {
            $data->result = $error;
        }

        return $data;
    }

    private function loadModel($name)
    {
        if ($this->models[$name]) {
            return $this->models[$name];
        }
        $path = '\App\Models\\'.$name;
        if (!class_exists($path)) {
            throw new \Exception('Bad model');
        }

        return new $path();
    }
}
