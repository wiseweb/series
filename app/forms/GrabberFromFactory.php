<?php

namespace App\Forms;

use Nette\Application\UI\Form;

class GrabberFormFactory
{
    /**
     * @var App\Forms\FormFactory
     */
    private $factory;
    public function __construct(FormFactory $factory)
    {
        $this->factory = $factory;
    }
    public function create()
    {
        $form = $this->factory->create();
        $form->addText('url', 'URL')
            ->setRequired()
            ->addRule(Form::URL, 'Zadaj správnu adresu odkazu');
        $form->addSubmit('grab', 'Grab');
        $form->addProtection('Security token has expired, please submit the form again');

        $renderer = $form->getRenderer();
        $renderer->wrappers['controls']['container'] = 'div';
        $renderer->wrappers['pair']['container'] = null;
        $renderer->wrappers['label']['container'] = null;
        $renderer->wrappers['control']['container'] = null;

        return $form;
    }
}
