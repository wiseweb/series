<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Episodes.
 *
 * @ORM\Table(name="episodes", indexes={@ORM\Index(name="source_id", columns={"source_id"})})
 * @ORM\Entity
 */
class Episodes
{
    /**
     * @var int
     *
     * @ORM\Column(name="episodeNumber", type="integer", nullable=false)
     */
    private $episodeNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="source_id", type="integer", nullable=false)
     */
    private $sourceId;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestamp", type="datetime", nullable=false)
     */
    private $timestamp = 'CURRENT_TIMESTAMP';

    /**
     * @var bool
     *
     * @ORM\Column(name="viewed", type="boolean", nullable=false)
     */
    private $viewed = '0';

    /**
     * @var \Serie
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\OneToOne(targetEntity="Serie")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id", referencedColumnName="episode_id")
     * })
     */
    private $id;

    /**
     * Set episodeNumber.
     *
     * @param int $episodeNumber
     *
     * @return Episodes
     */
    public function setEpisodeNumber($episodeNumber)
    {
        $this->episodeNumber = $episodeNumber;

        return $this;
    }

    /**
     * Get episodeNumber.
     *
     * @return int
     */
    public function getEpizodeNumber()
    {
        return $this->episodeNumber;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Episodes
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set sourceId.
     *
     * @param int $sourceId
     *
     * @return Episodes
     */
    public function setSourceId($sourceId)
    {
        $this->sourceId = $sourceId;

        return $this;
    }

    /**
     * Get sourceId.
     *
     * @return int
     */
    public function getSourceId()
    {
        return $this->sourceId;
    }

    /**
     * Set status.
     *
     * @param int $status
     *
     * @return Episodes
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set timestamp.
     *
     * @param \DateTime $timestamp
     *
     * @return Episodes
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get timestamp.
     *
     * @return \DateTime
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set viewed.
     *
     * @param bool $viewed
     *
     * @return Episodes
     */
    public function setViewed($viewed)
    {
        $this->viewed = $viewed;

        return $this;
    }

    /**
     * Get viewed.
     *
     * @return bool
     */
    public function getViewed()
    {
        return $this->viewed;
    }

    /**
     * Set id.
     *
     * @param \Serie $id
     *
     * @return Episodes
     */
    public function setId(\Serie $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return \Serie
     */
    public function getId()
    {
        return $this->id;
    }
}
