<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sources.
 *
 * @ORM\Table(name="sources", uniqueConstraints={@ORM\UniqueConstraint(name="link", columns={"link"})})
 * @ORM\Entity
 */
class Sources
{
    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=false)
     */
    private $link;

    /**
     * @var \Episodes
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Episodes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id", referencedColumnName="source_id")
     * })
     */
    private $id;

    /**
     * Set link.
     *
     * @param string $link
     *
     * @return Sources
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link.
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set id.
     *
     * @param \Episodes $id
     *
     * @return Sources
     */
    public function setId(\Episodes $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return \Episodes
     */
    public function getId()
    {
        return $this->id;
    }
}
