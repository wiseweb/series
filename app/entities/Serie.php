<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Serie.
 *
 * @ORM\Table(name="serie", uniqueConstraints={@ORM\UniqueConstraint(name="serieNumber", columns={"serieNumber", "episode_id"})}, indexes={@ORM\Index(name="episode_id", columns={"episode_id"})})
 * @ORM\Entity
 */
class Serie
{
    /**
     * @var int
     *
     * @ORM\Column(name="serieNumber", type="integer", nullable=false)
     */
    private $serieNumber;

    /**
     * @var int
     *
     * @ORM\Column(name="episode_id", type="integer", nullable=false)
     */
    private $episodeId;

    /**
     * @var \Serial
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Serial")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id", referencedColumnName="id")
     * })
     */
    private $id;

    /**
     * Set serieNumber.
     *
     * @param int $serieNumber
     *
     * @return Serie
     */
    public function setSerieNumber($serieNumber)
    {
        $this->serieNumber = $serieNumber;

        return $this;
    }

    /**
     * Get $serieNumber.
     *
     * @return int
     */
    public function getSerieNumber()
    {
        return $this->serieNumber;
    }

    /**
     * Set episodeId.
     *
     * @param int $episodeId
     *
     * @return Serie
     */
    public function setEpisodeId($episodeId)
    {
        $this->episodeId = $episodeId;

        return $this;
    }

    /**
     * Get episodeId.
     *
     * @return int
     */
    public function getEpisodeId()
    {
        return $this->episodeId;
    }

    /**
     * Set id.
     *
     * @param \Serial $id
     *
     * @return Serie
     */
    public function setId(\Serial $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return \Serial
     */
    public function getId()
    {
        return $this->id;
    }
}
