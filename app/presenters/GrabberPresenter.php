<?php

namespace App\Presenters;

class GrabberPresenter extends BasePresenter
{
    /**
     * @inject
     *
     * @var \App\Forms\GrabberFormFactory
     */
    public $factory;
    /**
     * @var App\Facade
     */
    private $facade;

    public function __construct(\App\Facade $facade)
    {
        $this->facade = $facade;
    }
    public function renderDefault()
    {
        if (!$this->getUser()->loggedIn) {
            $this->redirect('Sign:in');
        }
    }
    public function createComponentGrabberForm()
    {
        $form = $this->factory->create();

        return $form;
    }
    public function handleGrab($url)
    {
        $this->payload->response = $this->facade->grabURL($url, $this->user->id);
        $this->sendPayload();
    }
}
