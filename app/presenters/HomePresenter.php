<?php

namespace App\Presenters;

class HomePresenter extends BasePresenter
{
    /**
     * @var App\Facade
     */
    private $facade;
    public function __construct(\App\Facade $facade)
    {
        $this->facade = $facade;
    }
    public function actionDefault()
    {
        if (!$this->getUser()->loggedIn) {
            $this->redirect('Sign:in');
        }
    }
    /** RENDERERS */
    public function renderDefault()
    {
        $this->template->episodes = $this->facade->getNewUnwatchedEpisodes($this->user->id);
    }
    public function renderSeries(int $id)
    {
        $this->template->series = $this->facade->getSerialById($id);
    }
    public function renderSerials()
    {
        $this->template->serials = $serials = $this->facade->getSerials();
    }
    public function renderMySerials()
    {
        $this->template->serials = $serials = $this->facade->getMySerials($this->user->id);
    }

    public function renderEpisode(int $id)
    {
        $this->template->sources = $this->facade->getEpisode($id, $this->user->id);
    }

    /** HANDLERS */
    public function handleUnfavor(int $id)
    {
        $this->facade->unfavor($id, $this->user->id);
        $this->template->serials = $serials = $this->facade->getMySerials($this->user->id);
        $this->redrawControl('serials');
    }
    public function handleFavor(int $id)
    {
        $this->facade->favor($id, $this->user->id);
        $this->template->serials = $serials = $this->facade->getMySerials($this->user->id);
        $this->redrawControl('serials');
    }
    public function handleWatched(int $id, int $serialId)
    {
        $this->facade->markAsWatched($id, $this->user->id, $serialId);
        $this->redrawControl();
    }
    public function handleUnWatched(int $id)
    {
        $this->facade->markAsUnWatched($id, $this->user->id);
        $this->redrawControl();
    }
}
